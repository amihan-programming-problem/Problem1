class CharacterPrinter():
    allowedPatterns = ['O','X','Y','Z']
    size = 0
    pattern_input = None

    def __init__(self,pattern,size):
        
        self.size = size
        self.pattern_input = pattern
        
    
    def print(self):
        if self.pattern_input not in self.allowedPatterns:
            print(f'\nInvalid pattern input! Choose from {self.allowedPatterns}')
        
        elif (self.size%2 == 0 or self.size < 3):
            print('\nSize must be positive odd integers! Try again.')
        
        else:
            if self.pattern_input == 'Z':
                self.ZPattern()
            elif self.pattern_input == 'Y':
                self.YPattern()
            elif self.pattern_input == 'X':
                self.XPattern()
            else:
                self.OPattern()

    def ZPattern(self):
        for i in range(self.size):
            if i == 0 or i == self.size-1:
                print("*" * self.size)
            else:
                print(' ' * (self.size-i-1) + 
                      "*")

    def YPattern(self):
        for i in range(self.size):
            if i < self.size//2:
                print(' '*i + 
                      "*" + 
                      ' ' * ((self.size//2)-1-i) +
                      ' ' * ((self.size//2)-i) +
                      '*')
            else:
                print(' ' * ((self.size)//2) + 
                      '*')

    def XPattern(self):
        for i in range(self.size):
            if i < self.size//2:
                print(' ' * i + 
                      "*" + 
                      ' ' * ((self.size//2)-1-i) +
                      ' ' * ((self.size//2)-i) +
                      '*')

            elif i == self.size//2:
                print(' ' * ((self.size)//2) + '*')

            else:
                print(' ' * (self.size-i-1) + 
                      "*" + 
                      ' ' * ((i-(self.size//2)-1)) +
                      ' ' * (i-(self.size//2)) +
                      '*')

    def OPattern(self):
        for i in range(self.size):
            if (i==0 or i == self.size-1):
                print(
                    ' ' +
                    '*' * (self.size - 2))

            else:
                print(
                    '*' +
                    ' ' * (self.size - 2) +
                    '*')
        
if __name__ == "__main__":
    end = False
    while not end:
        pattern = str(input("> Enter a character [O, X, Y, Z]: "))
        size = int(input("> Enter a non-negative odd integer: "))
        output = CharacterPrinter(pattern,size)
        output.print()
        print()
        exit = input("Press Q to exit or any key to continue...\n")
        end = True if (exit == 'Q' or exit == 'q') else False
    
